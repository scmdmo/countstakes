# CountStakes

SpringBoot App based in a service oriented architecture (SOA).
Integrate with MongoDB (NoSQL) for persistence layer.
Use of Websocket without overlying protocol as STOMP. Direct text stream,
managing JSON messages on the Websocket. Possibility of Rest notifications to another system.

##Technologies and frameworks used
* Spring Boot App
* Spring Mongo Template
* Spring Rest Template
* Spring Websocket
* Jackson 
* RESTFul
* SOA
* Maven
* Docker
* JUnit

#Setup
This project was build in IntelliJ IDEA.

## Installing Database
NOSQL database (MongoDB) will be installed using Docker containers.
To install Docker:

`# apt-get install docker.io`

### MongoDB
To install MongoDB using docker and expose the default port run:

`# docker run -p 27017:27017 --name mx-mongo -d mongo`

Our container will be named mx-mongo.

##Configure 
###General config
Configuration is into the properties file *resources/application.properties*

Example of *application.properties* file

     app.name=CountStakes
     app.version=1.0
     slf4j.detectLoggerNameMismatch=true
     spring.data.mongodb.uri=mongodb://localhost:27017/betvictor
     
     #default options for initialize user accounts
     default.user.name=default name
     default.user.accumulate.stake.threshold=100
     default.user.accumulate.time.window=60
     
     #Notifications accumulated stake to external system.
     notification.enable=true
     notification.url=http://localhost:7777


###User account config
The time window and stake thresholds are configured for each user across the Users collection stored in Mongo.
 
Example of user record:

    {
     "_id" : ObjectId("5b588c4187754068ad5a9436"),
     "account" : NumberLong(200),
     "name" : "Jhon Doe",
     "accumulateStakeThreshold" : NumberLong(500),
     "accumulateStakeTimeWindow" : 1500,
     "_class" : "com.scm.countstakes.data.mongodo.UserDocument"
    }

##Build
Compile using maven and running the build jar.

#Action
The communication channel to the app is using websockets.
The websocket server is exposed in *ws://localhost:8080/stakes*

Whe a new client open a connection. The app send a message to identify itself in the form

`{"app.name":"CountStakes","app.version":"1.0"}`

The source message to the service are like
 
`{"account":100, "stake":10}`

When a new stake is sent to the app it is saved under collection Stakes in Mongo.  

Example stakes collection:
```json
{
 "_id" : ObjectId("5b58c8218775401d4a6bab5e"),
 "account" : NumberLong(100),
 "stake" : NumberLong(2),
 "time" : ISODate("2018-07-25T18:57:37.776Z"),
 "_class" : "com.scm.countstakes.data.mongodo.StakeDocument"
}

{
 "_id" : ObjectId("5b58c16b877540183c8f6563"),
 "account" : NumberLong(100),
 "stake" : NumberLong(15),
 "time" : ISODate("2018-07-25T18:28:59.628Z"),
 "_class" : "com.scm.countstakes.data.mongodo.StakeDocument"
}
```

If a new ingoing stake (account number) not match any user in collection Users. A new user account will make and initialize with the default user values set in the application properties file.

If the stake hit the threshold of accumulated bet during the time window defined. Application triggers a event that it will be saved in collection Events and if the external notification is enabled in the application properties file, it will be notified to a http url in form of POST request.

`{"account":100,"accumulatedStake":110}`

After all compute, a ACK message is sent back across websocket attaching the source message.

`{"ACK":{"account":100,"stake":10}}`


##Example of use
With user account 100 where it's properties are:
+ accumulateStakeThreshold : 100 
+ accumulateStakeTimeWindow : 60 (minutes)
    
And sending the next stream of stakes for a client inside 1 hour
since the first event.

    Server-> {"app.name":"CountStakes","app.version":"1.0"}
    Client-> {"account":100, "stake":5}
    Server-> {"ACK":{"account":100,"stake":5}}
    Client-> {"account":100, "stake":25}
    Server->{"ACK":{"account":100,"stake":25}}
    Client-> {"account":100, "stake":45}
    Server-> {"ACK":{"account":100,"stake":45}}
    Client-> {"account":100, "stake":15}
    Server-> {"ACK":{"account":100,"stake":15}}
    Client-> {"account":100, "stake":20}
    Server-> {"ACK":{"account":100,"stake":20}}
    Client-> {"account":100, "stake":20}
    Server-> {"ACK":{"account":100,"stake":20}}
    
It will produce the events

    {"account":100,"accumulatedStake":110}
    {"account":100,"accumulatedStake":140}
    
##Test tools
The operation can be tested of the application using a websocket client
to send the staks. For example a simply and effective client
can be the Chrome extension [*Simple WebSocket Client*](https://chrome.google.com/webstore/detail/simple-websocket-client/pfdhoblngboilpfeibdedpjgfnlcodoo) 

To check the content of mongoDB, you can use commanline tools *mongodb-clients*
or a GUI oriented app as [Robo 3T](www.robomongo.org) (formerly known Robomongo) 


To check the external POST notification. There is a mini server build in node.js and express.js under folder tools.
To run this, you should be to have installed node.js interpreter.Inside tools/event_test_tools run `npm install` to install dependencies.

After that, you can run the app with `npm start`. The node server will be listening on port 7777 by default.
You can change the listen port using the env var PORT. For example running
`PORT=5000 npm start`


 
