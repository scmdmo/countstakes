/**
 * Created by domingo on 25/07/18.
 */
'use strict';
const express = require('express');
const bodyParser = require('body-parser');
const app = express();

/**
 * Normalize a port into a number, string, or false.
 */
function normalizePort(val) {
    let port = parseInt(val, 10);
    if (isNaN(port)) {
        // named pipe
        return val;
    }
    if (port >= 0) {
        // port number
        return port;
    }
    return false;
}

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
        extended: true
    }));

app.post(['/','/event'], function (req, res) {
	console.log(`Ingoing EVENT-> ${JSON.stringify(req.body)}`);
    res.status(200).end();
});

const port = normalizePort(process.env.PORT || '7777')
app.listen(port, function () {
    console.log(`Event server app listening on port ${port}!`);
});
