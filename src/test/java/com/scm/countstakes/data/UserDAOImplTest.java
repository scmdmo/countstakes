package com.scm.countstakes.data;

import com.scm.countstakes.data.mongodo.UserDocument;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserDAOImplTest {

    @Autowired
    private UserDAO userDAO;
    @Autowired
    private MongoTemplate mongoTemplate;

    private UserDocument testUser;

    @Before
    public void setUp(){
        testUser = new UserDocument();
        testUser.setName("Test name");
        testUser.setAccount(9999L);
        testUser.setAccumulateStakeTimeWindow(45);
        testUser.setAccumulateStakeThreshold(5050);
        mongoTemplate.save(testUser);
    }

    @After
    public void tearDown(){
        Query q = new Query(new Criteria("account").is(testUser.getAccount()));
        mongoTemplate.remove(q,UserDocument.class);
    }

    @Test
    public void save() {
        userDAO.save(testUser);
    }

    @Test
    public void findUserByAccount() {
        UserDocument user = userDAO.findUserByAccount(9999L);
        assertThat(user.getName(), is(equalTo(testUser.getName())));
        assertThat(user.getAccount(), is(testUser.getAccount()));
        assertThat(user.getAccumulateStakeTimeWindow(),
                is(testUser.getAccumulateStakeTimeWindow()));
        assertThat(user.getAccumulateStakeThreshold(),
                is(testUser.getAccumulateStakeThreshold()));
        assertThat(user.toString(), is(equalTo(testUser.toString())));
    }
}