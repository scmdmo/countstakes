package com.scm.countstakes.data;

import com.scm.countstakes.data.mongodo.EventDocument;
import com.scm.countstakes.data.mongodo.TotalStake;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EventDAOImplTest {

    @Autowired
    private EventDAO eventDAO;
    @Autowired
    private MongoTemplate mongoTemplate;

    private EventDocument<TotalStake> eventDocument;

    @Before
    public void setUp() throws Exception {
        TotalStake totalStake = new TotalStake();
        totalStake.setAccount(100L);
        totalStake.setAccumulatedStake(205L);

        eventDocument = new EventDocument<>();
        eventDocument.setDescription("Test event");
        eventDocument.setValue(totalStake);
    }

    @After
    public void tearDown() throws Exception {
        Query q = new Query(new Criteria("value._id")
                .is(eventDocument.getValue().getAccount()));
        mongoTemplate.remove(q, EventDocument.class);
    }

    @Test
    public void save() {
        eventDAO.save(eventDocument);
        Query q = new Query(new Criteria("value._id")
                .is(eventDocument.getValue().getAccount()));
        EventDocument<TotalStake> testEvent = mongoTemplate.findOne(q, EventDocument.class);
        //TODO
        eventDocument.getValue();
        eventDocument.getDescription();
        eventDocument.getEventTime();
        System.out.println(eventDocument.toString());
    }
}