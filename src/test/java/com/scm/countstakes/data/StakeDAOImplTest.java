package com.scm.countstakes.data;

import com.scm.countstakes.data.mongodo.StakeDocument;
import com.scm.countstakes.data.mongodo.TotalStake;
import com.scm.countstakes.data.mongodo.UserDocument;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StakeDAOImplTest {

    @Autowired
    private StakeDAO stakeDAO;
    @Autowired
    private MongoTemplate mongoTemplate;

    private StakeDocument stakeDocument;
    private UserDocument userDocument;

    @Before
    public void setUp() throws Exception {
        stakeDocument = new StakeDocument(300L,5L);

        userDocument = new UserDocument();
        userDocument.setAccount(stakeDocument.getAccount());
        userDocument.setName("Someone");
        userDocument.setAccumulateStakeThreshold(100);
        userDocument.setAccumulateStakeTimeWindow(60);
    }

    @After
    public void tearDown() throws Exception {
        Query q = new Query(new Criteria("account").is(stakeDocument.getAccount()));
        mongoTemplate.remove(q,StakeDocument.class);
        mongoTemplate.remove(q,UserDocument.class);

    }

    @Test
    public void groupTotalStakeByAccountWithThreshold() {
        int j=3;
        for (int i=0; i<j; i++) {
            stakeDAO.save(new StakeDocument(
                    stakeDocument.getAccount(),
                    stakeDocument.getStake()));
        }
        TotalStake  totalStake = stakeDAO.groupTotalStakeByAccountWithThreshold(userDocument);
        assertThat(totalStake.getAccount(),is(stakeDocument.getAccount()));
        assertThat(totalStake.getAccumulatedStake(),is(stakeDocument.getStake()*j));
        System.out.println(stakeDocument.toString());
        System.out.println(totalStake.toString());
    }
}