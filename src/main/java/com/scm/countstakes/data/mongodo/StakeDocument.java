package com.scm.countstakes.data.mongodo;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection="Stakes")
public class StakeDocument {
    @Id
    private String id;
    private Long account;
    private Long stake;
    private Date time;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getAccount() {
        return account;
    }

    public void setAccount(Long account) {
        this.account = account;
    }

    public Long getStake() {
        return stake;
    }

    public void setStake(Long stake) {
        this.stake = stake;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public StakeDocument() {
    }

    public StakeDocument(Long account, Long stake) {
        this.account = account;
        this.stake = stake;
        this.time = new Date();
    }

    @Override
    public String toString() {
        return String.format(
                "Stake[id=%s, account='%d', stake='%d']",
                id, account, stake);
    }
}
