package com.scm.countstakes.data.mongodo;

import org.springframework.data.annotation.Id;

public class TotalStake {
    @Id
    private Long account;
    private Long accumulatedStake;

    public Long getAccount() {
        return account;
    }

    public void setAccount(Long account) {
        this.account = account;
    }

    public Long getAccumulatedStake() {
        return accumulatedStake;
    }

    public void setAccumulatedStake(Long accumulatedStake) {
        this.accumulatedStake = accumulatedStake;
    }

    @Override
    public String toString() {
        return String.format("TotalStake[account='%d', accumulatedStake='%d']",
                account, accumulatedStake);
    }
}