package com.scm.countstakes.data.mongodo;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="Users")
public class UserDocument {

    private Long account;
    private String name;
    private long accumulateStakeThreshold;
    private int accumulateStakeTimeWindow;

    public Long getAccount() {
        return account;
    }

    public void setAccount(Long account) {
        this.account = account;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getAccumulateStakeThreshold() {
        return accumulateStakeThreshold;
    }

    public void setAccumulateStakeThreshold(long accumulateStakeThreshold) {
        this.accumulateStakeThreshold = accumulateStakeThreshold;
    }

    public int getAccumulateStakeTimeWindow() {
        return accumulateStakeTimeWindow;
    }

    public void setAccumulateStakeTimeWindow(int accumulateStakeTimeWindow) {
        this.accumulateStakeTimeWindow = accumulateStakeTimeWindow;
    }

    @Override
    public String toString() {
        return "UserDocument{" +
                "account=" + account +
                ", name='" + name + '\'' +
                ", accumulateStakeThreshold=" + accumulateStakeThreshold +
                ", accumulateStakeTimeWindow=" + accumulateStakeTimeWindow +
                '}';
    }
}
