package com.scm.countstakes.data.mongodo;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection="Events")
public class EventDocument<T> {

    private String description;
    private Date eventTime = new Date();
    private T value;

    public Date getEventTime() {
        return eventTime;
    }

    public void setEventTime(Date eventTime) {
        this.eventTime = eventTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return String.format("Event[eventTime=%s, description='%s' , value='%s']",
                eventTime, description, value.toString());
    }
}
