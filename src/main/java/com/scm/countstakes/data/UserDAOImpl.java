package com.scm.countstakes.data;

import com.scm.countstakes.data.mongodo.UserDocument;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

@Repository
public class UserDAOImpl implements UserDAO{

    private MongoTemplate mongoTemplate;

    @Autowired
    public void setMongoTemplate(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public void save(UserDocument user) {
        mongoTemplate.save(user);
    }

    @Override
    public UserDocument findUserByAccount(Long account) {
        Query query = new Query();
        query.addCriteria(new Criteria("account").is(account));
        return mongoTemplate.findOne(query, UserDocument.class);
    }
}
