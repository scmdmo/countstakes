package com.scm.countstakes.data;

import com.scm.countstakes.data.mongodo.UserDocument;

public interface UserDAO {
    void save(UserDocument user);
    UserDocument findUserByAccount(Long account);
}
