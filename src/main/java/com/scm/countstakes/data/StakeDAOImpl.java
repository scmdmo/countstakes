package com.scm.countstakes.data;

import com.scm.countstakes.data.mongodo.StakeDocument;
import com.scm.countstakes.data.mongodo.TotalStake;
import com.scm.countstakes.data.mongodo.UserDocument;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.GroupOperation;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import java.util.Date;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;

@Repository
public class StakeDAOImpl implements StakeDAO {

    private MongoTemplate mongoTemplate;

    @Autowired
    public void setMongoTemplate(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public void save(StakeDocument stake) {
        mongoTemplate.save(stake);
    }

    @Override
    public TotalStake groupTotalStakeByAccountWithThreshold(UserDocument user) {
        /**
         * Native query
         * db.getCollection('Stakes').aggregate(
         *    {$match:{'account':124, 'time':{$gte:new Date(Date.now()-3600*1000*25)} }},
         *    {$group:{_id:'$account', 'accumulatedStake':{$sum:'$stake'}} }
         * )
         */

        Date time = new Date(System.currentTimeMillis()
                - (60*1000*user.getAccumulateStakeTimeWindow()));//milisec

        MatchOperation match = match(
                new Criteria("account").is(user.getAccount())
                        .and("time").gte(time));

        GroupOperation group = group("account").sum("stake")
                .as("accumulatedStake");

        Aggregation aggregation = Aggregation.newAggregation(match,group);

        AggregationResults<TotalStake> result = mongoTemplate.aggregate(
                aggregation, "Stakes", TotalStake.class);

        return result.getUniqueMappedResult();
    }
}
