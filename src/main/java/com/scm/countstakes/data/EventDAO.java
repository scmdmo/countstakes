package com.scm.countstakes.data;

import com.scm.countstakes.data.mongodo.EventDocument;

public interface EventDAO {
    void save(EventDocument event);
}
