package com.scm.countstakes.data;

import com.scm.countstakes.data.mongodo.StakeDocument;
import com.scm.countstakes.data.mongodo.TotalStake;
import com.scm.countstakes.data.mongodo.UserDocument;

public interface StakeDAO {

    void save(StakeDocument stake);
    TotalStake groupTotalStakeByAccountWithThreshold(UserDocument user);
}
