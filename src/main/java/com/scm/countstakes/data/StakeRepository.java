package com.scm.countstakes.data;

import com.scm.countstakes.data.mongodo.StakeDocument;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface StakeRepository extends MongoRepository<StakeDocument,String> {

}
