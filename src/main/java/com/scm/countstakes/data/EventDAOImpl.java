package com.scm.countstakes.data;

import com.scm.countstakes.data.mongodo.EventDocument;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class EventDAOImpl implements EventDAO {

    private MongoTemplate mongoTemplate;

    @Autowired
    public void setMongoTemplate(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public void save(EventDocument event) {
        mongoTemplate.save(event);
    }
}
