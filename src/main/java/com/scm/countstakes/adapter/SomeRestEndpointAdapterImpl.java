package com.scm.countstakes.adapter;

import com.scm.countstakes.data.mongodo.TotalStake;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;

@Component
public class SomeRestEndpointAdapterImpl implements SomeRestEndpointAdapter {
    private static final Logger logger = LoggerFactory.getLogger(SomeRestEndpointAdapterImpl.class);
    private RestTemplate restTemplate;
    {
        restTemplate = new RestTemplate(getClientHttpRequestFactory());
    }
    @Value("${notification.enable:false}")
    private boolean activate;
    @Value("${notification.url:http://localhost:7000}")
    private String url;

    @PostConstruct
    private void init(){
        if(!activate)
            logger.warn("Http notifications are disabled");
    }

    private ClientHttpRequestFactory getClientHttpRequestFactory() {
        int timeout = 5000;
        HttpComponentsClientHttpRequestFactory clientHttpRequestFactory=
                new HttpComponentsClientHttpRequestFactory();
        clientHttpRequestFactory.setConnectTimeout(timeout);
        return clientHttpRequestFactory;
    }

    @Override
    public void sendAccumulateEvent(TotalStake total){
        if(!activate)
            return;
        logger.info("Outgoing http notification -> {}",total);
        try {
            ResponseEntity<String> response = restTemplate.postForEntity(url, total,String.class);
            logger.info("Response code {}",response.getStatusCode());
            if (response.hasBody()) {
                logger.info("Response body {}", response.getBody());
            }
        }catch (RestClientException ex){
            logger.warn("Fail notification ",ex);
        }
    }
}
