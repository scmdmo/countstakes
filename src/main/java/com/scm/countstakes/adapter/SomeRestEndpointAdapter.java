package com.scm.countstakes.adapter;

import com.scm.countstakes.data.mongodo.TotalStake;

public interface SomeRestEndpointAdapter {
    void sendAccumulateEvent(TotalStake total);
}
