package com.scm.countstakes.business;

import com.scm.countstakes.dto.StakeMessageDTO;

public interface CountStakesBusiness {
    void computeStake(StakeMessageDTO stake);
}
