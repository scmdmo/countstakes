package com.scm.countstakes.business;

import com.scm.countstakes.adapter.SomeRestEndpointAdapter;
import com.scm.countstakes.data.EventDAO;
import com.scm.countstakes.data.StakeDAO;
import com.scm.countstakes.data.UserDAO;
import com.scm.countstakes.data.mongodo.EventDocument;
import com.scm.countstakes.data.mongodo.StakeDocument;
import com.scm.countstakes.data.mongodo.TotalStake;
import com.scm.countstakes.data.mongodo.UserDocument;
import com.scm.countstakes.dto.StakeMessageDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class CountStakesBusinessImpl implements CountStakesBusiness {
    private static final Logger logger = LoggerFactory.getLogger(CountStakesBusinessImpl.class);
    private StakeDAO stakeDAO;
    private UserDAO userDAO;
    private EventDAO eventDAO;
    private SomeRestEndpointAdapter endpointAdapter;
    private Environment env;

    @Autowired
    public void setStakeDAO(StakeDAO stakeDAO) {
        this.stakeDAO = stakeDAO;
    }

    @Autowired
    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @Autowired
    public void setEventDAO(EventDAO eventDAO) {
        this.eventDAO = eventDAO;
    }

    @Autowired
    public void setEndpointAdapter(SomeRestEndpointAdapter endpointAdapter) {
        this.endpointAdapter = endpointAdapter;
    }

    @Autowired
    public void setEnv(Environment env) {
        this.env = env;
    }

    @Override
    public void computeStake(StakeMessageDTO stake) {
        stakeDAO.save(new StakeDocument(stake.getAccount(),stake.getStake()));
        UserDocument userDocument = retrieveUserProfile(stake.getAccount());
        TotalStake totalStake = stakeDAO.groupTotalStakeByAccountWithThreshold(userDocument);
        logger.debug("totalStake: {}",totalStake);
        if(totalStake!=null && totalStake.getAccumulatedStake()>=userDocument.getAccumulateStakeThreshold()){
            EventDocument<TotalStake> event = new EventDocument<>();
            event.setValue(totalStake);
            event.setDescription("Reached accumulated stake threshold");
            eventDAO.save(event);
            //Launch Rest Notification
            endpointAdapter.sendAccumulateEvent(totalStake);
        }
    }

    private UserDocument retrieveUserProfile(Long account){
        UserDocument user = userDAO.findUserByAccount(account);
        if (user==null){
            logger.info("User account {} not exists. Initializing by default", account);
            user = new UserDocument();
            user.setAccount(account);
            long stakeThreshold = env.getProperty("default.user.accumulate.stake.threshold",Long.class,100L);
            user.setAccumulateStakeThreshold(stakeThreshold);
            int timeWindow = env.getProperty("default.user.accumulate.time.window",Integer.class,60);
            user.setAccumulateStakeTimeWindow(timeWindow);
            user.setName(env.getProperty("default.user.name","default name"));
            userDAO.save(user);
            logger.debug("Saved user account {}", user);
        }
        return user;
    }
}
