package com.scm.countstakes.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class AppVersion {
    @Value("${app.name}")
    private String name;
    @Value("${app.version}")
    private String version;

//    private static AppVersion;

    public String getName() {
        return name;
    }

    public String getVersion() {
        return version;
    }

    public Map<String,String> getVersionData() {
        HashMap map = new HashMap<String,String>();
        map.put("app.name",name);
        map.put("app.version",version);
        return map;
    }
}
