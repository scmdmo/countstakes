package com.scm.countstakes.websocket;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.scm.countstakes.dto.AckMessageDTO;
import com.scm.countstakes.dto.StakeMessageDTO;
import com.scm.countstakes.service.CountStakesService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import com.scm.countstakes.utils.AppVersion;

public class StakesWebSocketHandler extends TextWebSocketHandler {
    private static final Logger logger = LoggerFactory.getLogger(StakesWebSocketHandler.class);
    private List<WebSocketSession> sessions = new CopyOnWriteArrayList<>();
    private final ObjectMapper objectMapper = new ObjectMapper();
    private Map<String,String> version;
    private CountStakesService countStakesService;

    @Autowired
    public void setCountStakesService(CountStakesService countStakesService) {
        this.countStakesService = countStakesService;
    }

    @Autowired
    public void setVersion(AppVersion appVersion){
        version = appVersion.getVersionData();
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        super.afterConnectionEstablished(session);
        sessions.add(session);
        logger.debug("Websocket connection established id={}",session.getId());
        if(version!=null) {
            session.sendMessage(new TextMessage(
                    objectMapper.writeValueAsString(version)));
        }
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        super.afterConnectionClosed(session, status);
        logger.debug("Websocket connection closed id={} code={}",session.getId(),status.getCode());
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        super.handleTextMessage(session, message);
        logger.debug("Websocket ingoing message ->{}",message.getPayload());
        StakeMessageDTO stake = objectMapper.readValue(message.getPayload(), StakeMessageDTO.class);
        logger.info("Ingoing stake message ->{}", stake);

        countStakesService.computeStake(stake);
        session.sendMessage(new TextMessage(
                objectMapper.writeValueAsString(new AckMessageDTO<StakeMessageDTO>(stake))));
    }
}
