package com.scm.countstakes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CountstakesApplication {

    public static void main(String[] args) {
        SpringApplication.run(CountstakesApplication.class, args);
    }
}
