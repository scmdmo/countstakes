package com.scm.countstakes.dto;

public class StakeMessageDTO {
    private Long account;
    private Long stake;

    public Long getAccount() {
        return account;
    }

    public void setAccount(Long account) {
        this.account = account;
    }

    public Long getStake() {
        return stake;
    }

    public void setStake(Long stake) {
        this.stake = stake;
    }

    @Override
    public String toString() {
        return String.format("%s[account='%d', stake='%d']",this.getClass().getName(), account, stake);
    }
}
