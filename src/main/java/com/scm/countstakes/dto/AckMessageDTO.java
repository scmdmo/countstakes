package com.scm.countstakes.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AckMessageDTO<T> {
    @JsonProperty("ACK")
    private T value;

    public void setValue(T value) {
        this.value = value;
    }

    public T getValue() {
        return value;
    }

    public AckMessageDTO(T value) {
        this.value = value;
    }
}
