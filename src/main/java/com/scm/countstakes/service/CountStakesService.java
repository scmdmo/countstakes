package com.scm.countstakes.service;

import com.scm.countstakes.dto.StakeMessageDTO;

public interface CountStakesService {
    void computeStake(StakeMessageDTO stake);
}