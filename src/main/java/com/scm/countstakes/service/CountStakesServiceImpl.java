package com.scm.countstakes.service;

import com.scm.countstakes.business.CountStakesBusiness;
import com.scm.countstakes.dto.StakeMessageDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CountStakesServiceImpl implements CountStakesService {

    private CountStakesBusiness countStakesBusiness;

    @Autowired
    public void setCountStakesBusiness(CountStakesBusiness countStakesBusiness) {
        this.countStakesBusiness = countStakesBusiness;
    }

    @Override
    public void computeStake(StakeMessageDTO stake) {
        countStakesBusiness.computeStake(stake);
    }
}
